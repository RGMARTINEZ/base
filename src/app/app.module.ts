import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'
import { PerfilComponent } from './perfil/perfil.component';
import { ContactosComponent } from './contactos/contactos.component';

import { Routes, RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './material-module';
import { HttpClientModule } from '@angular/common/http';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { FormsModule } from '@angular/forms';


const appRoutes: Routes = [{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
}, 
{
  path: 'home',
  component: HomeComponent
},
{
    path: 'perfil',
    component: PerfilComponent
}, 
{
    path: 'contactos',
    component: ContactosComponent
}];


@NgModule({
  declarations: [
    AppComponent,
    PerfilComponent,
    ContactosComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    DemoMaterialModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
