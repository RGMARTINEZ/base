import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.css']
})

export class ContactosComponent implements OnInit {
  animal: string;
  name: string;
  titulo: string;
  datos: any = [];
  libro: any = {
    imagen: '', nombre: '', direccion: '', telefono: null
  }
  id: number;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.datos = [{
      imagen: 'https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg',
      nombre: 'Sortino',
      direccion: 'Accountant',
      telefono: '+447662552550'
    }];
    }
  


  nuevo(): void  {
    this.titulo = 'Nuevo contacto'
    this.libro = {
      imagen: '', nombre: '', direccion: '', telefono: null
    }

  }

  guardar(): void  {
    if (this.titulo === 'Editar contacto'){
      this.datos[this.id] = this.libro;

    } else  {
      if (this.libro.imagen !== ''){
        this.datos.push(this.libro);
      }
      
    }
    
  }

  editar(payload, index) {
    this.id = index;
    this.titulo = 'Editar contacto'
    this.libro = {
      imagen: payload.imagen, nombre: payload.nombre, direccion: payload.direccion, telefono: payload.telefono
    }
  }


}


