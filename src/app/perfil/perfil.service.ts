import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import { Observable } from 'rxjs';

export class TaskList {
    nombre: string;
    telefono: string;
    direccion: string;
    imagen: any;
  }

  
@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  constructor(private http: HttpClient) {
  }

  public getPerfil():Observable<TaskList> {
    return this.http.get<TaskList>( 'assets/data/perfil.json')
      .pipe(
        retry(1)
      );
  }

}
