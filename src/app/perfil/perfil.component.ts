import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PerfilService } from './perfil.service'

export class TaskList {
  nombre: string;
  telefono: string;
  direccion: string;
  imagen: any;
}

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  providers: [PerfilService]
})
export class PerfilComponent implements OnInit {
  public data: any = {};
  constructor(private info: PerfilService, public httpClient: HttpClient) { }

  ngOnInit() {
    this.obtenerPerfil();
  }

  obtenerPerfil() {
    return this.info.getPerfil().subscribe((data: {}) => {
      this.data = data;
      console.log(this.data)
    })
  }

}
